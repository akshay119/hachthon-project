const mongoose = require('mongoose');
const lookup = require('./lookup');

// Schema for a MongoDB collection//

const FeedbackField = new mongoose.Schema({
    data: {
        type: Object,
        required: true,
    },
    status: {
        type: String,
        enum: lookup.type.status,
        default: ""
    }
});

module.exports = mongoose.model('feedback', FeedbackField);