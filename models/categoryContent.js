const mongoose = require('mongoose');
const lookup = require('./lookup');

// Schema for a MongoDB collection//

const categoryContent = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    content:{
        type: ObjectId,
        ref: 'content'
    },
    status: {
        type: String,
        enum: lookup.type.status,
        default: ""
    }
});

module.exports = mongoose.model('feedback', FeedbackField);