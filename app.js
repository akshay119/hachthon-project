const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

const contentSchema = require('./models/content');
const categorySchema = require('./models/categoryContent');
const feedbackSchema = require('./models/feedback');
const dbConnect = require('./db/db')

//Database Connection
if (dbConnect) console.log('Connected to DataBase');

//Middleware
app.use(express.json());
app.use(cors());
app.use(bodyParser.json());


//APIs

//Create Data
app.post('/insert', async (req, res) => {
    try {
        const newSchema = new contentSchema(req.body);
        const newFeedbackSchema = new feedbackSchema(req.body);

        if (!newSchema && !newFeedbackSchema) {
            res.status(400).json({ "status": "404", "message": "Require body request" });
        }
        await newSchema.save();
        await newFeedbackSchema.save();
        res.status(200).json({ "status": "200", "message": "Successfully Created" });
        return;
    } catch (err) {
        res.send({ "status": "500", error: err });
        return;
    }
});

//Read Data
app.get('/read', async (req, res) => {
    try {
        let contentData = await contentSchema.find();
        let feedbackData = await feedbackSchema.find();
        let data = {
            contentData,
            feedbackData
        }
        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({ error: 'An error occurred while fetching data' });
    }
});


// Update Data
app.put('/update/content/:id', async (req, res) => {
    const { id } = req.params;

    try {

        const data = req.body;
        if (data) {
            const updatedContent = await contentSchema.findByIdAndUpdate({ _id: id }, { data: data.data, status: data.status });

            if (!updatedContent) {
                return res.status(400).json({ error: 'Content not found' });
            }
        }
        res.status(200).json({ "status": "200", "message": "Successfully Updated" });
    } catch (err) {
        res.status(500).json({ error: 'An error occurred while updating data' });
    }
});

app.put('/update/feedback/:id', async (req, res) => {
    const { id } = req.params;

    try {

        const data = req.body;
        if (data) {
            const updatedFeedback = await feedbackSchema.findByIdAndUpdate({ _id: id }, { data: data.data, status: data.status });
            if (!updatedFeedback) {
                return res.status(400).json({ error: 'Feedback not found' });
            }
        }
        res.status(200).json({ "status": "200", "message": "Successfully Updated" });
    } catch (err) {
        res.status(500).json({ error: 'An error occurred while updating data' });
    }
});

// //Delete Data
app.delete('/delete/content/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const data = req.body;
        if (data) {
            const deleteContent = await contentSchema.findByIdAndRemove(id).exec();;
            if (!deleteContent) {
                return res.status(400).json({ error: 'Content not found' });
            }
        }
        res.status(200).json({ "status": "200", "message": "Successfully deleted" });
    } catch (err) {
        res.status(500).json({ error: 'An error occurred while deleting data' });
    }
});

app.delete('/delete/feedback/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const data = req.body;
        if (data) {
            const deleteFeedback = await feedbackSchema.findByIdAndRemove(id).exec();;
            if (!deleteFeedback) {
                return res.status(400).json({ error: 'Feedback not found' });
            }
        }
        res.status(200).json({ "status": "200", "message": "Successfully deleted" });
    } catch (err) {
        res.status(500).json({ error: 'An error occurred while deleting data' });
    }
});

app.post('/category/create', async (req, res) => {
    try {
        const newSchema = new categorySchema(req.body);
        // const newFeedbackSchema = new feedbackSchema(req.body);

        if (!newSchema ) {
            res.status(400).json({ "status": "400", "message": "Require body request" });
        }
        await newSchema.save();
        await newFeedbackSchema.save();
        res.status(200).json({ "status": "200", "message": "Successfully Created" });
        return;
    } catch (err) {
        res.send({ "status": "500", error: err });
        return;
    }
});



//HealthCheck
app.get('/healthcheck', async (req, res) => {
    res.send('Server running successfully')
});

//Hosting at PORT
const PORT = 6000;
app.listen(PORT, () => console.log('Listening at Port ' + PORT));